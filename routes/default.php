<?php
$app->get(
    '/'
    , function () use ($app) {
    $app->render('home.php');
});

$app->get(
    '/tag'
    , function () use ($app) {
    echo json_encode(App::getTags());
});

$app->post(
    '/tag'
    , function () use ($app) {

    $postdata = json_decode(file_get_contents("php://input"));
    $now = date('Y-m-d H:i:s');

    $tags = \RedBeanPHP\R::dispense('tags');
    $tags->title = $postdata->title;
    $tags->created = $now;
    $tags->modified = $now;

    $tagID = \RedBeanPHP\R::store($tags);

    if ($tagID) {
        echo json_encode(1);
    } else {
        echo json_encode(0);
    }
});