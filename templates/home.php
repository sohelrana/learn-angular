<!DOCTYPE html>
<html lang="en" ng-app="gemStore">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title;?></title>

    <link rel="stylesheet" href="<?php echo $baseUrl; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>assets/css/style.css">

    <script src="<?php echo $baseUrl; ?>assets/js/jquery.js"></script>
    <script src="<?php echo $baseUrl; ?>assets/js/bootstrap.js"></script>
    <script src="<?php echo $baseUrl; ?>assets/js/angular.js"></script>
    <script src="<?php echo $baseUrl; ?>assets/js/app.js"></script>
</head>
<body>

    <div class="container margin-top-100" ng-controller="TagController as tagCtrl">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tag</h3>
                    </div>
                    <div class="panel-body">
                        <form name="addNewTagForm" ng-submit="addNewTag(tag)">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tag</label>
                                <input ng-model="tag.title" type="text" class="form-control" placeholder="Type your tag">
                            </div>
                            <button type="submit" class="btn btn-danger">Save</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <h2>Tag List</h2>
                <hr class=""/>
                <table class="table table-bordered">
                    <tr>
                        <th>Title</th>
                        <th>Created</th>
                    </tr>
                    <tr ng-repeat="tag in tags">
                        <td>{{tag.title}}</td>
                        <td>{{tag.created}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</body>
</html>