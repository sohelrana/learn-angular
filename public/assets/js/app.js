(function() {
    var app = angular.module('gemStore', []);

    app.controller('TagController', ['$scope', '$http', function($scope, $http) {

        $http.get("tag")
            .success(function(tags)
            {
                $scope.tags = tags;
            }
        );

        $scope.tag = '';
        $scope.addNewTag = function(tag) {

            console.log(tag);

            $http({
                url: 'tag',
                method: "POST",
                data: tag,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
                .success(function (data, status, headers, config) {
                console.log(data);
            })
                .error(function (data, status, headers, config) {
                console.log(data);
            });

            $scope.tags.push(tag);
            $scope.tag = '';
        };
    }]);



})();
