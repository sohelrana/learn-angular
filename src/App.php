<?php
/**
 * Created by PhpStorm.
 * User: sobuz
 * Date: 11/14/2015
 * Time: 5:55 AM
 */

class App
{
    public static $baseUrl = 'http://localhost/learn-angular/public/';

    public static function getTags()
    {
        $tags = \RedBeanPHP\R::getAll( 'SELECT * FROM tags' );
        return $tags;
    }
}